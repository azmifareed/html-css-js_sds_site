<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><jdoc:include type="head" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SDS Spices</title>
<script src="<?php echo $this->baseurl; ?>/templates/sds-home/js/jquery.ad-gallery.js" type="text/javascript"></script>
<link href="<?php echo $this->baseurl; ?>/templates/sds-home/css/default.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->baseurl; ?>/templates/sds-home/css/jquery.ad-gallery.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<link href="<?php echo $this->baseurl; ?>/templates/sds-home/css/tabcontent.css" rel="stylesheet" type="text/css" />
<!--[if IE 7]>
<link href="<?php echo $this->baseurl; ?>/templates/sds-home/css/default_ie7.css" rel="stylesheet" type="text/css" />
<![endif]-->
<link rel="shortcut icon" type="image/x-icon" href="<?php echo $this->baseurl; ?>/images/favicon.ico" />
<script src="<?php echo $this->baseurl; ?>/templates/sds-home/js/jquery.ad-gallery.pack.js" type="text/javascript"></script>
<script src="<?php echo $this->baseurl; ?>/templates/sds-home/js/tabcontent.js" type="text/javascript"></script>
<script src="<?php echo $this->baseurl; ?>/templates/sds-home/js/gen_validatorv4.js" type="text/javascript"></script>


<script type="text/javascript" src="<?php echo $this->baseurl; ?>/templates/sds-home/jquery/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
<script type="text/javascript" src="<?php echo $this->baseurl; ?>/templates/sds-home/jquery/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl; ?>/templates/sds-home/jquery/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
<script type="text/javascript">
    $(document).ready(function() {

      $("#various1").fancybox({
        'titlePosition'    : 'inside',
        'transitionIn'    : 'none',
        'transitionOut'    : 'none'
      });

    });
</script>
<script type="text/javascript">

	if(/Safari/.test(navigator.userAgent))
	{
 		//you are using safari, or at least you CLAIM to be
		document.write('<link type="text/css" media="screen" rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/sds-home/css/style_safari.css" title="example" />');
	}
	</script>
</head>


<body>
<div id="container">
  <div id="header">
    <div id="top-links">
			<jdoc:include type="modules" name="top_menu" />
    </div>
  <div class="clr"></div>
      
      <div id="mainmenuL">
			<jdoc:include type="modules" name="main_menu_left" />
      </div>
      <div id="mainmenuR">
			<jdoc:include type="modules" name="main_menu_right" />
      </div>
    <div class="clr"></div>
    <div id="logo"><a href="<?php echo $this->baseurl; ?>"><img src="images/logo.png" width="121" height="93" /></a></div>
  </div>