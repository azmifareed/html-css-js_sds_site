# README #

This repository contains the XHTML/CSS files developed to convert the PSD layouts of the SDS Spices website. Some sliders were also implemented in some pages using JQuery and JavaScript.

Developed in 2012


### Contribution ###

* All of the files in this repository were created by myself

### Contact ###

* For information contact Azmi at mafareed@gmail.com